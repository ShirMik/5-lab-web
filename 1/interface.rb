# frozen_string_literal: true

require_relative 'equator'

def enter_x
  puts 'Введите значение x: '
  x = gets.to_i
  x = x.to_i
  puts Equator.calc_y(x)
end

enter_x if __FILE__ == $PROGRAM_NAME
