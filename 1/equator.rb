# frozen_string_literal: true

# if x is incorrect it returns nil
# 1 is impossible result of this calculation
# it proven by mathematics
class Equator
  include Math

  def self.calc_y(x)
    return unless x.is_a? Numeric

    Math.sqrt((Math.sin((x**3 + x**2).abs)**3) / \
      (PI + (x**3 + x**2 - x)**2))
  end
end
