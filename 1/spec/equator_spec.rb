# frozen_string_literal: true

require_relative '../equator'
require 'faker'

RSpec.describe Equator do
  describe '#check' do
    context 'when param is number' do
      let(:param) { Faker::Number.number(digits: 4) / 100 }

      it 'it should process -1 as 0.0' do
        expect(described_class.calc_y(-1)).to eq(0.0)
      end
      it 'it should process 55 as 0.0' do
        expect(described_class.calc_y(10)).to eq(0.0002571229704797911)
      end
      it 'it should process random number as float' do
        expect(described_class.calc_y(param)).to be_an(Float)
      end
    end

    context 'when param is string' do
      let(:param) { Faker::Lorem.word }

      it 'it should process word as nil' do
        expect(described_class.calc_y(param)).to eq(nil)
      end
    end
  end
end
