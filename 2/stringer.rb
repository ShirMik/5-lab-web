# frozen_string_literal: true

# this class contains method that processes string
class Stringer
  include Enumerable

  def self.process_string(string)
    return unless string.is_a? String

    string.split('').select { |char| char if /(\d|[a-z])/.match(char) }.join('')
  end

  def self.polynd?(string)
    return unless string.is_a? String

    string == string.reverse
  end

  def self.process_polynd(string)
    polynd?(process_string(string))
  end
end
