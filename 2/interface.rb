# frozen_string_literal: true

require_relative 'stringer'

def enter_x
  print 'Введите строку: '
  str = gets
  puts Stringer.process_string(str)

  if Stringer.process_polynd(str)
    puts 'Это определённо особый палиндром'
  else
    puts 'Это определённо не особый палиндром'
  end
end

enter_x if __FILE__ == $PROGRAM_NAME
