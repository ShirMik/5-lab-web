# frozen_string_literal: true

require_relative '../stringer'
require 'faker'

RSpec.describe Stringer do
  def make_random_string
    result = (1..2).collect { Faker::Lorem.word.downcase }.inject(:+)
    result.reverse != result ? result : make_random_string
  end

  def inject_illegal_symbols(string)
    string.split(' ').collect { |x| "#{x}#{Faker::Lorem.character.upcase}" }.join(' ')
  end

  describe '#check' do
    context 'when param is string' do
      let(:param) { make_random_string }

      it 'check for fixed value' do
        param = 'a 24 dfDV-0 23xew'
        expect(described_class.process_string(param)).to \
          eq('a24df023xew')
      end

      it 'check for fixed value polyndrom' do
        param = 'a 24 DVS 42 WEF      a'
        expect(described_class.process_polynd(param)).to \
          be_truthy
      end

      it 'check for random value clear transition' do
        expect(described_class.process_string(param)).to \
          eq(param)
      end

      it 'check for random value with injected illegal symbols' do
        expect(described_class.process_string(inject_illegal_symbols(param))).to \
          eq(param)
      end
    end
  end
end
