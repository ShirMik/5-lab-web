# frozen_string_literal: true

# Дана последовательность строк, содержащих слова. В конце каждой - точка.
# Слова образуют пары, где первое слово будет заменено в ходе
# корректировки вторым. По итогу тестировщик должен генерить
# рандомные данные для проверки согласно описанным правилам.
# Вывести исходную и скорректированную строку.

# this class contains method that processes string
class Liner
  include Enumerable

  def self.process_line(string)
    return unless (string.is_a? String) && (string[-1] == '.')

    "#{string[1..-2].split(' ').collect.with_index do |val, i|
      "#{val} #{val}" if i.odd?
    end.compact.join(' ')}."
  end

  def self.process_lines(lines)
    return unless lines.is_a? String

    lines.split("\n").collect do |line|
      process_line(line)
    end.compact.join("\n")
  end
end
