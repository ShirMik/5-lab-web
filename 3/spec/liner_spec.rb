# frozen_string_literal: true

require_relative '../liner'
require 'faker'

describe Liner do
  def check_line(string)
    include Enumerable
    return unless (string.is_a? String) && (string[-1] == '.')

    string.scan(/([^\s]+)\s([^\s.]+)/).all? { |pair| pair[0] == pair[1] }
  end

  def check_lines(lines)
    include Enumerable
    return unless lines.is_a? String

    lines.split("\n").all? { |line| check_line line }
  end

  def get_rand_line(sentence)
    sentence.split(' ').collect { |st| "#{Faker::Lorem.word} #{st}" }.join(' ')
  end

  def get_rand_result(sentence)
    sentence.split(' ').collect { |x| "#{x[-1] != '.' ? x : x[0..-2]} #{x}" }.join(' ')
  end

  describe '#check' do
    context 'when param is predefined' do
      it 'testing one line function and several lines function with words of the same length' do
        param = 'ab ba dc da ga ag fh fh.'
        result = 'ba ba da da ag ag fh fh.'

        expect(described_class.process_line(param)).to eq(result)
        expect(described_class.process_lines(param)).to eq(result)
      end

      it 'testing one line function and several lines function with words of the different length' do
        param = 'bab bads darcgg dasacgac agcasgxasdg agacsgxgaxdgaaxg fhaxgsdxgacgdxacg fhax.'
        result = 'bads bads dasacgac dasacgac agacsgxgaxdgaaxg agacsgxgaxdgaaxg fhax fhax.'

        expect(described_class.process_line(param)).to eq(result)
        expect(described_class.process_lines(param)).to eq(result)
      end

      it 'testing multiple lines function with words of the different length' do
        param = "bab bads darcgg dasacgac.\nagcasgxasdg agacsgxgaxdgaaxg.\nfhaxgsdxgacgdxacg fhax."
        result = "bads bads dasacgac dasacgac.\nagacsgxgaxdgaaxg agacsgxgaxdgaaxg.\nfhax fhax."

        expect(described_class.process_lines(param)).to eq(result)
      end
    end
    context 'when param is random line' do
      let(:sentence) { Faker::Lorem.sentence }
      let(:param) { get_rand_line(sentence) }
      let(:result) { get_rand_result(sentence) }

      it 'testing one line with random words' do
        expect(described_class.process_line(param)).to eq(result)
        expect(described_class.process_lines(param)).to eq(result)
        expect(check_lines(described_class.process_line(param))).to be_truthy
        expect(check_lines(described_class.process_lines(param))).to be_truthy
      end
    end

    context 'when param is several lines' do
      include Enumerable
      let(:sentence) { Faker::Lorem.sentence }
      let(:lines_number) { Faker::Number.number(digits: 2) }
      let(:param) { [1..lines_number].collect { get_rand_line(sentence) }.join("\n") }
      let(:result) { [1..lines_number].collect { get_rand_result(sentence) }.join("\n") }

      it 'testing several lines function with random words' do
        expect(described_class.process_lines(param)).to eq(result)
        expect(check_lines(described_class.process_lines(param))).to be_truthy
      end
    end
  end
end
