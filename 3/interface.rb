# frozen_string_literal: true

require_relative 'liner'

def enter_lines
  print 'Введите строку: '
  ln = gets.chomp
  puts Liner.process_line(ln)
end

enter_lines if __FILE__ == $PROGRAM_NAME
